import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StoredataService {

  constructor() { }

  employee = [
    { "id": "1", "name": "Muthu", "gender": "Male", "age": 27 },
    { "id": "2", "name": "Venkat", "gender": "Male", "age": 27 },
    { "id": "3", "name": "Baskar", "gender": "Male", "age": 26 },
    { "id": "4", "name": "Vijay", "gender": "Male", "age": 27 },
  ];

  country = [
    { "name": "India", "city": "Chennai", "population": "7 crore", "temperature": "40", "code": "91" },
    { "name": "USA", "city": "washington dc", "population": "13 crore", "temperature": "30", "code": "101" },
    { "name": "China", "city": "beijing", "population": "10 crore", "temperature": "35", "code": "81" },
  ];

  /**
   * Return employee detail
   */
  getEmployeeDetail(): Observable<any> {
    let data = new Observable<any>(observer => {
      observer.next(this.employee);
    });
    return data;
  }
  /**
   * Return country detail
   */
  getCountryDetail():Observable<any>{
    let data = new Observable<any>(observer=>{
      observer.next(this.country);
    });
    return data;
  }

}
