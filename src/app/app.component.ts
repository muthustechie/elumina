import { Component, OnInit } from '@angular/core';
import {StoredataService} from './storedata.service';
import { from } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{

  constructor(private storedata:StoredataService){}

  title = 'elumina';
  list;
  keys;

  /**
   * Get employee list from service
   */
  getEmployeeList(){
   this.setValueNull();
   this.storedata.getEmployeeDetail().subscribe(
    data=>{
      this.list =data;
      this.keys = Object.keys(this.list[0]);
    }
  );
  }

  /**
   * Get country list from seervice
   */
  getCountryList(){
    this.setValueNull();
    this.storedata.getCountryDetail().subscribe(
      data=>{
        this.list = data;
        this.keys = Object.keys(this.list[0]);
      }
    )
  }
  
  /**
   * Set value NULL for reuse
   */
  setValueNull(){
    this.list = null;
    this.keys = null;
  }

  ngOnInit(){
    this.getEmployeeList();
  }
}
